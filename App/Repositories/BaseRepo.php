<?php
namespace App\Repositories;

abstract class BaseRepo {
	protected $model ;


	public function insert($data) {
		return $this->model::insert($data);
	}

	public function find($id) {
		return $this->model::find($id);

	}

	public function findAll() {
		return $this->model::findAll();

	}

	public function update($data,$id) {
		return $this->model::update($data,$id);
	}

	public function delete($id) {
		return $this->model::delete($id);

	}
}