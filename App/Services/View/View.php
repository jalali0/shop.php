<?php
namespace App\Services\View;

class View{
	public static function load($viewPath,$data = array(),$layout = null) {
		$viewPath = str_replace(".","/",$viewPath);
		$fullViewPath = VIEWS.$viewPath.".php";
		if(file_exists($fullViewPath) and is_readable($fullViewPath)){
			extract($data);
			if($layout == null){
				include $fullViewPath;
			}else{
				$fullLayoutPath = VIEWS."layout/".$layout.".php";
				$view = View::render($viewPath);
				include $fullLayoutPath;
			}
		}
	}
	public static function render($viewPath,$data = array()) {
		$viewPath = str_replace(".","/",$viewPath);
		$fullViewPath = VIEWS.$viewPath.".php";
		if(file_exists($fullViewPath) and is_readable($fullViewPath)){
			extract($data);
			ob_start();
			include $fullViewPath;
			$renderedView = ob_get_clean();
			return $renderedView;
		}
	}

}